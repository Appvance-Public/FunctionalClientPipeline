/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.appvance.ci.example;

import com.appvance.ci.executer.testdesigner.TestDesginerBridge;
import com.appvance.ci.functional.Importance;
import com.appvance.ci.functional.ImportanceLevel;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author luis
 */
public class BasicExample {

    static {
        com.appvance.ci.executer.Configuration.baseURL = "http://localhost:9090/ds/dyn/";
 
    }

    @Test
    public void httpNavigation() throws Exception {
        TestDesginerBridge bridge = new TestDesginerBridge();
        bridge.processScript("src/com/appvance/ci/example/sahitest/sahitest.ds", "Chrome");
    }
    

    @Test
    public void httpsNavigation() throws Exception {
        TestDesginerBridge bridge = new TestDesginerBridge();
        bridge.processScript("src/com/appvance/ci/example/demosite/demosite.ds", "Firefox");
    }

    @Test(dependsOnMethods = "httpNavigation")
    public void thirdTest() throws Exception {
        //TestDesginerBridge bridge = new TestDesginerBridge();
        //bridge.setDPL(DPLType.Hash, "src/cirunnertest/data.csv");
        //bridge.processScript("src/cirunnertest/demosite.ds", "Firefox");
    }
    
    
    

    @Test
    @Importance(level = ImportanceLevel.high)
    public void highImportanceTest() throws Exception {
        Assert.assertTrue(true,"This should always succeed");
    }

    @Test
    @Importance(level = ImportanceLevel.low)
    public void lowImportanceTest() throws Exception {
        Assert.assertTrue(false,"This should always fail");
    }
}
